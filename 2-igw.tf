# ინტერნეტ gateway ინტერნეტში გასასვლელად და შემომავალი ტრაფიკების მისაღებად
# ეს ყველაფერი მიმაგრებულია aws_vpc_main ზე 
resource "aws_internet_gateway" "igw"{
    vpc_id = aws_vpc.main.id

    tags ={
        Name = "igw"
    }
}