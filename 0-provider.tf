
terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "~> 4.0"
      }
    }
    
}
# ვუთითებთ ინსტანსის რეგიონს.
provider "aws"{
    region = "eu-central-1"
}

# განვსაზღვრავთ ცვლადებს.ვუთითებთ კლასტერის სახელს 
variable "cluster_name"{
    default = "devops-eks-terraform"
}
# ვუთითებთ კლასტერის ვერსიას.
variable "cluster_version"{
    default = "1.27"
}
